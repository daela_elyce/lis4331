> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Mobile App Development 

## Daela Lyewsang Holness

### Assignment 2 Requirements:

*Two Parts:*
1. Drop-down menu for total number of guests (including yourself): 1 –10

2. Drop-down menu for tip percentage (5% increments): 0 –253.Must add background color(s) or theme(10 pts)4.Must create and displaylauncher icon image(10 pts)
#### README.md file should include the following items:
* Screenshot of running application’s unpopulated user interface;
* Screenshot of running application’s populated user interface;

#### Assignment Screenshots:

*Screenshot of running application’s unpopulated user interface*:

![Unpopulated](img/fillscreen.png)

*Screenshot of running application’spopulateduser interface*:

![JDK Installation Screenshot](img/uppop.PNG)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
