package com.example.alohamusic;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PlayMusic extends AppCompatActivity {
    Button button1, button2,button3;
    MediaPlayer mpProtoje, mpChronixx,mpBob;
    int playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_music);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar() .setLogo(R.mipmap.ic_launcher);
        getSupportActionBar() .setDisplayUseLogoEnabled(true);
        button1 = (Button) findViewById(R.id.btnProtoje);
        button2 = (Button) findViewById(R.id.btnChronixx);
        button3 = (Button) findViewById(R.id.btnBob);
        button1.setOnClickListener(bProtoje);
        button2.setOnClickListener(bChronixx);
        button3.setOnClickListener(bBob);
        mpProtoje = new MediaPlayer();
        mpProtoje = MediaPlayer.create(this, R.raw.protoje);
        mpChronixx = new MediaPlayer();
        mpChronixx = MediaPlayer.create(this, R.raw.chronixx);
        mpBob = new MediaPlayer();
        mpBob = MediaPlayer.create(this, R.raw.redemption );
        playing = 0;
    }
    Button.OnClickListener bProtoje = new Button.OnClickListener(){

        @Override
        public void onClick(View v) {
            switch(playing){
                case 0:
                    mpProtoje.start();
                    playing=1;
                    button1.setText("Pause Who Knows");
                    button2.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.INVISIBLE);
                    break;
                 case 1:
                     mpProtoje.pause();
                     playing = 0;
                     button1.setText("Play Who Knows");
                     button2.setVisibility(View.VISIBLE);
                     button3.setVisibility(View.VISIBLE);
                     break;

            }

        }
    };
    Button.OnClickListener bChronixx = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            switch(playing){
                case 0:
                    mpChronixx.start();
                    playing=1;
                    button2.setText("Pause Chronixx");
                    button1.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpChronixx.pause();
                    playing = 0;
                    button2.setText("Play Chronixx");
                    button1.setVisibility(View.VISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    break;

            }
        }
    };
    Button.OnClickListener bBob = new Button.OnClickListener(){

        @Override
        public void onClick(View v) {
            switch(playing){
                case 0:
                    mpBob.start();
                    playing=1;
                    button3.setText("Pause Bob Marley");
                    button1.setVisibility(View.INVISIBLE);
                    button2.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpBob.pause();
                    playing = 0;
                    button3.setText("Play Bob Marley");
                    button1.setVisibility(View.VISIBLE);
                    button2.setVisibility(View.VISIBLE);
                    break;

            }
        }
    };

}
