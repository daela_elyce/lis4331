> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course Title

## Daela Lyewsang-Holness

### Assignment 3 Requirements:
    - Field to enter U.S. dollar amount: 1–100,000
    - Must include toast notification if user enters out-of-range values
    - Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
    - Must include correct sign for euros, pesos, and Canadian dollars
    - Must add background color(s) or theme
    - Create and displaylauncher icon image

#### Assignment Screenshots:

*Screenshot of unpopulated userface*:

![AMPPS Installation Screenshot](img/capture1.PNG)

*Screenshotofrunning application’s toast notification;*:

![JDK Installation Screenshot](img/capture2.PNG)

*Screenshot of running application’s converted currency user interface*:

![Android Studio Installation Screenshot](img/capture3.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
