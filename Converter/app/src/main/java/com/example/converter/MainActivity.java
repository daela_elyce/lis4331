package com.example.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    double conversionRateEuro = 0.88;
    double conversionRatePeso = 19.19;
    double conversionRateCan = 1.32;
    double weightEntered;
    double convertedWeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        final EditText weight =(EditText) findViewById(R.id.txtWeight);
        final RadioButton lbToKilo = (RadioButton) findViewById(R.id.radLbToKilo);
        final RadioButton kiloToLb= (RadioButton) findViewById(R.id.radKiloToLb);
        final RadioButton radUsToCan= (RadioButton) findViewById(R.id.radUsToCan);
        final TextView result = (TextView) findViewById(R.id.txtResult);
        Button convert=(Button)findViewById(R.id.btnConvert);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weightEntered = Double.parseDouble(weight.getText().toString());
                DecimalFormat tenth = new DecimalFormat("#.#");
                if (lbToKilo.isChecked()) {
                    if (weightEntered <= 100000) {
                        convertedWeight = weightEntered * conversionRateEuro;
                        result.setText("€" + tenth.format(convertedWeight));
                    } else {
                        Toast.makeText(MainActivity.this, "Us Dollar must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                    }
                if (kiloToLb.isChecked()) {

                    if (weightEntered <= 100000) {
                        convertedWeight = weightEntered * conversionRatePeso;
                        result.setText("mex$" + tenth.format(convertedWeight));
                    } else {
                        Toast.makeText(MainActivity.this, "Us Dollar must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                        }
                if(radUsToCan.isChecked()){
                    if (weightEntered <=100000){
                        convertedWeight = weightEntered * conversionRateCan;
                        result.setText("Can$" + tenth.format(convertedWeight));
                    }else{
                        Toast.makeText(MainActivity.this, "Us Dollar must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }
                    }

                }}
            }


            );}
    }