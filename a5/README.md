> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Mobile App Development

## Daela Lyewsang-Holness

### Assignment 4 Requirements:


1. Images and buttons must be vertically and horizontally aligned.
2. Must add background color(s) or theme
3. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of running application’s splash screen
* Screenshot of running application’s follow up screen (with images and buttons)
* Screenshot of running application’s play and pause user interfaces (with images and buttons)




#### Assignment Screenshots:

*Screenshot of Splash Screen*:

|*Screenshot1*:  |  *Screenshot2*: |
|-----------------------------:|:----------------------------------|
|![P1 Carousel](img/screenshot1.png "Failed server side validaton") | ![P1 Failed](img/screenshot2.png "Passed Server Side Validation") |




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
