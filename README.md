**NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4331

## Daela Lyewsang

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    -Provide screenshots of installations
    -create bitbucket repo
    - complete bitbucket tutorials
    provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Drop-down menu for total number of guests (including yourself): 1 –10
    - Drop-down menu for tip percentage (5% increments): 0 –25
    - Must add background color(s) or theme(10 pts)
    - Must create and displaylauncher icon image(10 pts)
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Field to enter U.S. dollar amount: 1–100,000
    - Must include toast notification if user enters out-of-range values
    - Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
    - Must include correct sign for euros, pesos, and Canadian dollars
    - Must add background color(s) or theme
    - Create and displaylauncher icon image


3. [P1 README.md](a4/README.md "My P1 README.md file")
    - Include splash screen image, app title, intro text.
    - Include artists’ images and mediag
    - Images and buttons must be vertically and horizontally aligned.
    - Must add background color(s) or theme
    - Create and display launcher icon image


4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Include splash screen image, app title, intro text.
    - Include artists’ images and media
    - Images and buttons must be vertically and horizontally aligned.
    - Must add background color(s) or theme
    - Create and display launcher icon image

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Include splash screen image, app title, intro text.
    - Include artists’ images and media
    - Images and buttons must be vertically and horizontally aligned.
    - Must add background color(s) or theme
    - Create and display launcher icon imag

6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Include splash screen image, app title, intro text.
    - Include artists’ images and media
    - Images and buttons must be vertically and horizontally aligned.
    - Must add background color(s) or theme
    - Create and display launcher icon imag
