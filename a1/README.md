> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4331 Mobile App Development

## Daela Lyewsang

### Assignment 1 Requirements:

*Four Parts:*

1. Distrubuted version controls
2. Development Installations
3. Chapter Questions(Chs. 1,2)
4. Bitbucket repo links a)this assignment and b) the compeleted tutorials

#### README.md file should include the following items:                       *Git commands w/short descriptions:  
|*Screenshot of running java Hello                                   |1. git init-initialize and empty git repo                                |
------------------------------------------------------------------------------------------------------------------------------------------------
|*Screen Shot of Andriod- My First App                               |2.git status-  displays state of working directory                       |
------------------------------------------------------------------------------------------------------------------------------------------------
|*Screen Shot of Andriod- Contacts App                               |3. git add- adds files                                                   |
------------------------------------------------------------------------------------------------------------------------------------------------
|*Git commands with short descriptions                               |4. git commit- commits files to local repo                               |
------------------------------------------------------------------------------------------------------------------------------------------------
|                                                                    |5. git push- uploads local content to remote repo                        |
------------------------------------------------------------------------------------------------------------------------------------------------
|                                                                    |6. git pull- fetches and downloads files from remote repo to local repo  |
------------------------------------------------------------------------------------------------------------------------------------------------
|                                                                    |7. git remote- synchs changes                                            |
------------------------------------------------------------------------------------------------------------------------------------------------

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:


*Screenshot of running java Hello*:                           *Screenshot of Android Studio MyFirst App:*

![JDK Installation Screenshot](./img/jdk_inst.png)          

![AndroidStu Installation Screenshot](./img/firstapp.PNG)    

*Screenshot of Contact App-Main Screen*:                     *Screenshot of Contacts App- Info Screen

![Android Studio Main Contact Screen](./img/contact2.PNG)       ![Android Studio Info Contact Screen](./img/contacts.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
